//------------------------------------------------------------------------
// Quick and dirty tile map class.
// Feel free to use this for your entry if you want but you don't have to.
// If you do not use this then you should provide an alternative that represents a pac-man style map.
//------------------------------------------------------------------------
#include "stdafx.h"
//------------------------------------------------------------------------
#include <windows.h> 
#include <math.h>  
#include <assert.h>  
//------------------------------------------------------------------------
#include "app\app.h"
#include "SimpleTileMap.h"

static const int NUM_MAP_DEFS = 7;

// Color mapping for render. Indexed based on tile value.
static float g_tileMapDef[NUM_MAP_DEFS][4]
{
    // Red, Green, Blue, Scale.
	{ 0.0f,0.0f,0.5f, 1.0f }, // Border
	{ 0.0f,0.0f,0.5f, 1.0f }, // Wall
	{ 0.8f,0.8f,0.0f, 0.15f}, // Pip or in other words, floor
	{ 0.0f,0.0f,0.0f, 0.15f}, // Blank floor, zero out space from pip after collection
	{ 1.0f,1.0f,1.0f, 0.5f }, // Power Pellet    
	{ 0.0f,1.0f,1.0f, 1.0f }, // ?
	{ 0.0f,0.0f,1.0f, 1.0f }, // ?
};

// Direction lookup used by random map generation.
static int g_dirLookup[4][2] =
{
    { -1, 0 },
    {  1, 0 },
    {  0,-1 },
    {  0, 1 }
};

void CSimpleTileMap::Create()
{    
    m_tileMap.clear();
    m_tileMap.resize(m_mapSize);
    for (int i = 0; i < m_mapSize; i++)
        m_tileMap[i].resize(m_mapSize, EMapValue::WALL);

    /*m_tileWidth = (float)APP_VIRTUAL_WIDTH / (float)m_mapSize;
    m_tileHeight = (float)APP_VIRTUAL_HEIGHT / (float)m_mapSize;*/

	m_tileWidth = (float)APP_VIRTUAL_WIDTH / (float)m_mapSize;
	m_tileHeight = (float)APP_INGAME_MAP_HEIGHT / (float)m_mapSize;
}

void CSimpleTileMap::Clear(EMapValue clearValue)
{    
    for (auto &row : m_tileMap)
    {
        row.assign(row.size(), clearValue);        
    }
}

bool CSimpleTileMap::SetTileMapValue(const int x, const int y, const EMapValue v)
{
    if ((x >= 0 && x < m_mapSize) && (y >= 0 && y < m_mapSize))
    {
        m_tileMap[x][y] = v;
        return true;
    }
    return false;
}

bool CSimpleTileMap::SetTileMapValue(const Vector2 &position, EMapValue v)
{
	int x = (int)(position.x / m_tileWidth);
	int y = (int)(position.y / m_tileHeight);
	return SetTileMapValue(x, y, v);
}

EMapValue CSimpleTileMap::GetTileMapValue(const int x, const int y) const
{
    if ((x >= 0 && x < m_mapSize) && (y >= 0 && y < m_mapSize))
    {
        return m_tileMap[x][y];
    }
    return EMapValue::OUTOFBOUNDS;
}

EMapValue CSimpleTileMap::GetTileMapValue(const float fx, const float fy) const
{
    int x = (int)(fx / m_tileWidth);
    int y = (int)(fy / m_tileHeight);
    return GetTileMapValue(x, y);
}

EMapValue CSimpleTileMap::GetTileMapValue(const Vector2 &position) const
{
	int x = (int)(position.x / m_tileWidth);
	int y = (int)(position.y / m_tileHeight);
	return GetTileMapValue(x, y);
}

void CSimpleTileMap::Render() const
{
    float xStep = m_tileWidth;
    float yStep = m_tileHeight;
    for (int y = 0; y < m_mapSize; y++)
    {
        for (int x = 0; x < m_mapSize; x++)
        {
            int index = GetTileMapValue(x, y) % NUM_MAP_DEFS;
            float scale = g_tileMapDef[index][3];
            float xPos = (x * xStep);            
            xPos += (xStep - (xStep*scale))/2.0f;

            float yPos = (y * yStep);
            yPos += (yStep - (yStep*scale)) / 2.0f;

            float w = xStep * g_tileMapDef[index][3];
            float h = yStep * g_tileMapDef[index][3];

            App::DrawQuad(xPos, yPos, xPos + w, yPos + h, g_tileMapDef[index][0], g_tileMapDef[index][1], g_tileMapDef[index][2]);
        }
    }
}

//------------------------------------------------------------------------
// Randomly creates tunnels through the map.
// Picks a direction then moves in a random direction of length (0-maxTunnelLength)
// Picks new direction and repeats until we have filled the map with the targetFloorPercentage of FLOOR tiles.
//------------------------------------------------------------------------
void CSimpleTileMap::RandomMap(const float targetFloorPercentage, const int maxTunnelLength)
{
    // Clear the map as WALLs then fill the BORDERs.
    float percentageOfFloorCovered = 0.0f;
    float oneTilesPercentage = 100.0f / (float)(m_mapSize * m_mapSize);

    Clear();

    // Set the borders.
    for (int y = 0; y < m_mapSize; y++)
    {
        SetTileMapValue(0, y, EMapValue::BORDER);
        SetTileMapValue(m_mapSize-1, y, EMapValue::BORDER); 
        percentageOfFloorCovered += oneTilesPercentage*2;
    }
    for (int x = 1; x < m_mapSize-1; x++)
    {
        SetTileMapValue(x, 0, EMapValue::BORDER);
        SetTileMapValue(x, m_mapSize - 1, EMapValue::BORDER);
        percentageOfFloorCovered += oneTilesPercentage * 2;
    }    
    int lastDir = rand() % 4;               // Last direction we filled the map in.
    int currentRow = 1 + (rand() % (m_mapSize-2));
    int currentColumn = 1 + (rand() % (m_mapSize-2));
          
    while( percentageOfFloorCovered < targetFloorPercentage )
    {
        int currentDir = GetNewDirection(currentRow, currentColumn, lastDir);         // Get a new random direction.
        int randomLength = 1 + rand() % (maxTunnelLength-1); //length the next tunnel will be (max of maxLength)
        for (int l = 0; l < randomLength; l++)
        {               
            // If the next step will take you into a BORDER of the grid then change direction.
            if (GetTileMapValue(currentRow + g_dirLookup[currentDir][0], currentColumn + g_dirLookup[currentDir][1]) == EMapValue::BORDER)
            {
                currentDir = GetNewDirection(currentRow, currentColumn, currentDir);
            }
            lastDir = currentDir;
            currentRow += g_dirLookup[currentDir][0];
            currentColumn += g_dirLookup[currentDir][1];
            if (GetTileMapValue(currentRow, currentColumn) < EMapValue::PIP)
            {
                percentageOfFloorCovered += oneTilesPercentage;
            }

			SetTileMapValue(currentRow, currentColumn, EMapValue::PIP);
        }        
    }

	ScatterPowerups();
}

Vector2 CSimpleTileMap::GetSpawnPosition(int playerNumber)
{
	Vector2 spawnAxis{ Vector2::zero };
	Vector2 spawnPos{ Vector2::zero };


	switch (playerNumber)
	{
	case 1:
	{
		for (int x = 0; x < m_mapSize - 1; ++x)
		{
			if (m_tileMap[x][m_mapSize - 2] >= EMapValue::PIP)
			{
				spawnAxis.x = x;
				spawnAxis.y = m_mapSize - 2;
				break;
			}
		}

		break;
	}
	case 2:
	{
		for (int x = m_mapSize - 1; x > 0; --x)
		{
			if (m_tileMap[x][m_mapSize - 2] >= EMapValue::PIP)
			{
				spawnAxis.x = x;
				spawnAxis.y = m_mapSize - 2;
				break;
			}
		}

		break;
	}
	case 3:
	{
		for (int x = 0; x < m_mapSize - 1; ++x)
		{
			if (m_tileMap[x][1] >= EMapValue::PIP)
			{
				spawnAxis.x = x;
				spawnAxis.y = 1;
				break;
			}
		}

		break;
	}
	case 4:
	{
		for (int x = m_mapSize - 1; x > 0; --x)
		{
			if (m_tileMap[x][1] >= EMapValue::PIP)
			{
				spawnAxis.x = x;
				spawnAxis.y = 1;
				break;
			}
		}

		break;
	}
	}
	
	float scale = g_tileMapDef[m_tileMap[spawnAxis.x][spawnAxis.y]][3];
	float xPos = (spawnAxis.x * m_tileWidth) + ((m_tileWidth - (m_tileWidth * scale)) / 2.0f);
	float yPos = (spawnAxis.y * m_tileHeight) + ((m_tileHeight - (m_tileHeight * scale)) / 2.0f);

	spawnPos.x = xPos;
	spawnPos.y = yPos;

	return spawnPos;
}

Vector2 CSimpleTileMap::SnapToMapGrid(const Vector2 & vec)
{
	int x = (int)(vec.x / m_tileWidth);
	int y = (int)(vec.y / m_tileHeight);

	if (GetTileMapValue(x, y) < EMapValue::PIP)
	{
		if (GetTileMapValue(x - 1, y) >= EMapValue::PIP)
		{
			x -= 1;
		}
		else if (GetTileMapValue(x + 1, y) >= EMapValue::PIP)
		{
			x += 1;
		}
		else if (GetTileMapValue(x, y - 1) >= EMapValue::PIP)
		{
			y -= 1;
		}
		else if (GetTileMapValue(x, y + 1) >= EMapValue::PIP)
		{
			y += 1;
		}
		else if (GetTileMapValue(x - 1, y - 1) >= EMapValue::PIP)
		{
			x -= 1;
			y -= 1;
		}
		else if (GetTileMapValue(x + 1, y + 1) >= EMapValue::PIP)
		{
			x += 1;
			y += 1;
		}
		else if (GetTileMapValue(x - 1, y + 1) >= EMapValue::PIP)
		{
			x -= 1;
			y += 1;
		}
		else if (GetTileMapValue(x + 1, y - 1) >= EMapValue::PIP)
		{
			x += 1;
			y -= 1;
		}
	}

	float scale = g_tileMapDef[m_tileMap[x][y]][3];
	float xPos = (x * m_tileWidth) + ((m_tileWidth - (m_tileWidth * scale)) / 2.0f);
	float yPos = (y * m_tileHeight) + ((m_tileHeight - (m_tileHeight * scale)) / 2.0f);

	Vector2 snappedPos{ xPos, yPos };
	return snappedPos;
}

Vector2 CSimpleTileMap::GetRandomPosition()
{
	Vector2 foundPos{ Vector2::zero };

	while (foundPos == Vector2::zero)
	{
		int x = rand() % m_mapSize;
		int y = rand() % m_mapSize;

		if (GetTileMapValue(x, y) >= EMapValue::PIP)
		{
			float scale = g_tileMapDef[m_tileMap[x][y]][3];
			float xPos = (x * m_tileWidth) + ((m_tileWidth - (m_tileWidth * scale)) / 2.0f);
			float yPos = (y * m_tileHeight) + ((m_tileHeight - (m_tileHeight * scale)) / 2.0f);

			foundPos.x = xPos;
			foundPos.y = yPos;
		}
	}

	return foundPos;
}

bool CSimpleTileMap::GetNextPowerupPosition(Vector2 & outPosition)
{
	if (powerupLocations.size() > 0)
	{
		Vector2 location = powerupLocations[rand() % powerupLocations.size()];
		float scale = g_tileMapDef[m_tileMap[location.x][location.y]][3];
		float xPos = (location.x * m_tileWidth) + ((m_tileWidth - (m_tileWidth * scale)) / 2.0f);
		float yPos = (location.y * m_tileHeight) + ((m_tileHeight - (m_tileHeight * scale)) / 2.0f);

		outPosition.x = xPos;
		outPosition.y = yPos;
		return true;
	}

	return false;
}

void CSimpleTileMap::OnPowerupCollected(const Vector2 &location)
{
	int x = (int)(location.x / m_tileWidth);
	int y = (int)(location.y / m_tileHeight);
	
	for (auto itLocations = powerupLocations.begin(); itLocations != powerupLocations.end(); ++itLocations)
	{
		if ((*itLocations).x == x && (*itLocations).y == y)
		{
			powerupLocations.erase(itLocations);
			break;
		}
	}

	if (powerupLocations.size() == 0)
	{
		ScatterPowerups();
	}
}

void CSimpleTileMap::ScatterPowerups()
{
	if (powerupLocations.size() > 1) { return; }

	int numPowerupsToSpawn = static_cast<int>(m_mapSize * 0.25f);
	while (numPowerupsToSpawn > 0)
	{
		int spawnX = rand() % (m_mapSize - 1) + 1;
		int spawnY = rand() % (m_mapSize - 1) + 1;

		if (GetTileMapValue(spawnX, spawnY) >= EMapValue::PIP)
		{
			powerupLocations.push_back(Vector2(spawnX, spawnY));
			SetTileMapValue(spawnX, spawnY, EMapValue::POWERPELLET);
			--numPowerupsToSpawn;
		}
	}
}

void CSimpleTileMap::ScatterPellets()
{
	for (int x = 0; x < m_tileMap.size(); ++x)
	{
		for (int y = 0; y < m_tileMap[x].size(); ++y)
		{
			if (GetTileMapValue(x, y) == EMapValue::FLOOR)
			{
				SetTileMapValue(x, y, EMapValue::PIP);
			}
		}
	}
}

int CSimpleTileMap::GetNewDirection(const int currentRow, const int currentColumn, int currentDir) const
{
    // Get random direction. Perp to the last direction.
    int newDir = rand() % 2;
    if (currentDir < 2)
        newDir += 2;
    //If the new tile direction would hit a border then go in the opposite direction.
    if (GetTileMapValue(currentRow + g_dirLookup[newDir][0], currentColumn + g_dirLookup[newDir][1]) == EMapValue::BORDER)
    {
        switch (newDir)
        {
        case 0:
            return 1;
        case 1:
            return 0;
        case 2:
            return 3;
        case 3:
            return 2;
        }
    }
    return newDir;
}
