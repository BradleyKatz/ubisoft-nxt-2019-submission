#include "stdafx.h"
#include "Pathfinding.h"
#include "..\SimpleTileMap.h"

extern CSimpleTileMap g_map;

/* 
====================================
FILE PRIVATE DEFINITIONS AND METHODS
====================================
*/

namespace
{
	struct Tile
	{
	public:
		float weight;
		float distFromParent;
		float distFromGoal;
		Vector2 position;
		Tile *parent;

		Tile() : weight{ 0.0f }, distFromParent{ 0.0f }, distFromGoal{ 0.0f }, position{ Vector2::zero }, parent{ nullptr }
		{
		}
	};

	float GetDistToGoal(Tile *tile, const Vector2 &goal)
	{
		if (tile != nullptr)
		{
			tile->distFromGoal = Vector2::Distance(tile->position, goal);
			return tile->distFromGoal;
		}
		else
		{
			return 0.0f;
		}
	}

	float GetDistToParent(Tile *tile)
	{
		if (tile != nullptr)
		{
			float distToParent = 0.0f;
			Tile *parentTile = tile->parent;

			while (parentTile != nullptr)
			{
				distToParent += 10.0f;
				parentTile = parentTile->parent;
			}

			tile->distFromParent = distToParent;
			return tile->distFromParent;
		}
		else
		{
			return 0.0f;
		}
	}

	void AddTileToOpenList(const float x, const float y, Tile *parent, std::vector<Tile*> &openList)
	{
		Tile *tile = new Tile();

		Vector2 snappedPos = g_map.SnapToMapGrid(Vector2(x, y));

		tile->position.x = snappedPos.x;
		tile->position.y = snappedPos.y;
		tile->parent = parent;
		tile->weight = 0.0f;
		tile->distFromGoal = 0.0f;
		tile->distFromParent = 0.0f;

		openList.push_back(tile);
	}

	Tile* GetTileInClosedList(const Vector2 &position, std::map<float, Tile*> &closedList)
	{
		for (auto itTile = closedList.begin(); itTile != closedList.end(); ++itTile)
		{
			//if (Vector2::Distance((*itTile).second->position, position) <= 0.5f/*g_map.GetTileWidth() / 2.0f*/)
			if ((*itTile).second->position == position)
			{
				return (*itTile).second;
			}
		}

		return nullptr;
	}

	bool IsValidTile(const float x, const float y, Tile *currentTile, std::map<float, Tile*> &closedList)
	{
		if (currentTile == nullptr)
		{
			return false;
		}

		Vector2 position{ x, y };

		// Is the current tile we're considering walkable?
		if (g_map.GetTileMapValue(position) >= EMapValue::PIP)
		{
			Tile *visitedTile = GetTileInClosedList(position, closedList);
			if (visitedTile != nullptr)
			{
				if (visitedTile->distFromParent > (currentTile->distFromParent + 10))
				{
					for (auto itTile = closedList.begin(); itTile != closedList.end(); ++itTile)
					{
						if ((*itTile).second->position == position)
						{
							closedList.erase(itTile);
							return true;
						}
					}

					return false;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	void GetAdjacentNodes(Tile *currentTile, std::vector<Tile*> &openList, std::map<float, Tile*> &closedList)
	{
		if (currentTile != nullptr)
		{
			float tileWidth = g_map.GetTileWidth();
			float tileHeight = g_map.GetTileHeight();

			if (IsValidTile(currentTile->position.x, currentTile->position.y + tileHeight, currentTile, closedList))
			{
				AddTileToOpenList(currentTile->position.x, currentTile->position.y + tileHeight, currentTile, openList);
			}

			if (IsValidTile(currentTile->position.x, currentTile->position.y - tileHeight, currentTile, closedList))
			{
				AddTileToOpenList(currentTile->position.x, currentTile->position.y - tileHeight, currentTile, openList);
			}

			if (IsValidTile(currentTile->position.x + tileWidth, currentTile->position.y, currentTile, closedList))
			{
				AddTileToOpenList(currentTile->position.x + tileWidth, currentTile->position.y, currentTile, openList);
			}

			if (IsValidTile(currentTile->position.x - tileWidth, currentTile->position.y, currentTile, closedList))
			{
				AddTileToOpenList(currentTile->position.x - tileWidth, currentTile->position.y, currentTile, openList);
			}
		}
	}

	void InitPath(const Vector2 &position, const Vector2 &target, std::vector<Tile*> &openList, std::map<float, Tile*> &closedList)
	{
		// Begin at the starting point A and add it to the open list
		Tile *startingTile = new Tile();
		startingTile->position.x = position.x;
		startingTile->position.y = position.y;
		startingTile->parent = nullptr;
		startingTile->weight = 0.0f;

		openList.push_back(startingTile);

		// Look at all the reachable or walkable nodes adjacent to the starting point, 
		// ignoring nodes with walls or other obstacles and them to the open list
		// For each of these nodes, save the starting point A as the parent node.
		GetAdjacentNodes(startingTile, openList, closedList);
		for (Tile *tile : openList)
		{
			tile->weight = GetDistToParent(tile) + GetDistToGoal(tile, target);
		}

		// Drop the starting node A from the open list, move it to the closed list
		for (auto itOpen = openList.begin(); itOpen != openList.end(); ++itOpen)
		{
			if ((*itOpen)->position == startingTile->position)
			{
				openList.erase(itOpen);
				break;
			}
		}

		closedList.insert(std::make_pair(startingTile->weight, startingTile));
	}

	Tile* GetNextCheapestTile(std::vector<Tile*> &openList, std::map<float, Tile*> &closedList)
	{
		if (openList.size() > 0)
		{
			auto itCheapest = openList.begin();

			for (auto itOpen = openList.begin() + 1; itOpen != openList.end(); ++itOpen)
			{
				if ((*itOpen)->weight < (*itCheapest)->weight)
				{
					itCheapest = itOpen;
				}
			}

			Tile *cheapest = (*itCheapest);
			openList.erase(itCheapest);

			for (auto itClosed = closedList.begin(); itClosed != closedList.end(); ++itClosed)
			{
				if ((*itClosed).second->position == cheapest->position)
				{
					closedList.erase(itClosed);
					break;
				}
			}

			closedList.insert(std::make_pair(cheapest->weight, cheapest));
			return cheapest;
		}
		else
		{
			return nullptr;
		}
	}

	void BuildPathToTarget(const Vector2 &target, std::map<float, Tile*> &closedList, std::vector<Vector2> &outPath)
	{
		Tile *targetTile = GetTileInClosedList(target, closedList);
		if (targetTile != nullptr)
		{
			outPath.insert(outPath.begin(), targetTile->position);

			Tile *nextTile = targetTile->parent;
			while (nextTile != nullptr)
			{
				outPath.insert(outPath.begin(), nextTile->position);
				nextTile = nextTile->parent;
			}
		}
	}
}
/*
=========================
PATHFINDING CLASS METHODS
=========================
*/

PathFinding& PathFinding::GetInstance()
{
	static PathFinding instance;
	return instance;
}

void PathFinding::GeneratePath(const Vector2 &position, const Vector2 &target, std::vector<Vector2> &outPath)
{
	std::vector<Tile*> openList;
	std::map<float, Tile*> closedList;

	Vector2 start = g_map.SnapToMapGrid(position);
	Vector2 end = g_map.SnapToMapGrid(target);

	if (start == end)
	{
		return;
	}

	InitPath(start, end, openList, closedList);

	Tile *nextCheapestTile = nullptr;
	while ((nextCheapestTile = GetNextCheapestTile(openList, closedList)) != nullptr && GetTileInClosedList(end, closedList) == nullptr)
	{
		GetAdjacentNodes(nextCheapestTile, openList, closedList);
		for (Tile *tile : openList)
		{
			tile->weight = GetDistToParent(tile) + GetDistToGoal(tile, target);
		}
	}

	BuildPathToTarget(end, closedList, outPath);
	openList.clear();
	closedList.clear();
}