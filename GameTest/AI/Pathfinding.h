#ifndef _PATHFINDING_H
#define _PATHFINDING_H

#include "..\Common\Vector2.h"

class PathFinding
{
public:
	static PathFinding &GetInstance();

	void GeneratePath(const Vector2 &position, const Vector2 &target, std::vector<Vector2> &outPath);
};

#endif