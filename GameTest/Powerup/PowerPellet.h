#ifndef _POWERPELLET_H
#define _POWERPELLET_H

#include "PowerupBase.h"

class PowerPellet : public PowerupBase
{
	friend class PlayerManager;
	
public:
	PowerPellet();
	~PowerPellet();

	void Update(float deltaTime) override;
};

#endif