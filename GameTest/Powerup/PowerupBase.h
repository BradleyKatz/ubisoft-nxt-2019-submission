#ifndef _POWERUPBASE_H
#define _POWERUPBASE_H

#include <string>
#include "..\Common\Color.h"

class PowerupBase
{
	friend class PlayerManager;
	friend class PlayerBase;

protected:
	// Duration = time in seconds powerup should last
	float duration, elapsedTime;
	Color color;
	std::string name;

public:
	PowerupBase();
	PowerupBase(float duration, float elapsedTime, const Color &color, const std::string &name);
	~PowerupBase();

	virtual void Render();
	virtual void Update(float deltaTime);
};

#endif
