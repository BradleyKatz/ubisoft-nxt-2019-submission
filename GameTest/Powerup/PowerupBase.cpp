#include "stdafx.h"
#include "PowerupBase.h"

PowerupBase::PowerupBase() : duration{ 0.0f }, elapsedTime{ 0.0f }, color{ Color::black }, name{ "" }
{
}

PowerupBase::PowerupBase(float duration, float elapsedTime, const Color & color, const std::string & name) : duration{ duration }, elapsedTime{ 0.0f }, color{ color }, name{ name }
{
}

PowerupBase::~PowerupBase()
{
}

void PowerupBase::Render()
{
}

void PowerupBase::Update(float deltaTime)
{
}