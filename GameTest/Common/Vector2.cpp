#include "stdafx.h"

#include <cmath>

#include "Vector2.h"

const Vector2 Vector2::down{ 0.0f, -1.0f };
const Vector2 Vector2::up{ 0.0f, 1.0f };
const Vector2 Vector2::left{ -1.0f, 0.0f };
const Vector2 Vector2::right{ 1.0f, 0.0f };
const Vector2 Vector2::zero{ 0.0f, 0.0f };

Vector2::Vector2() : x{ 0.0f }, y{ 0.0f }
{
}

Vector2::Vector2(float x, float y) : x{ x }, y{ y }
{
}

Vector2::Vector2(const Vector2 &vec) : x{ vec.x }, y{ vec.y }
{
}

//Vector2::~Vector2()
//{
//}

Vector2 Vector2::operator+(const Vector2 &v2)
{
	return Vector2(x + v2.x, y + v2.y);
}

Vector2 Vector2::operator-(const Vector2 &v2)
{
	return Vector2(x - v2.x, y - v2.y);
}

Vector2 Vector2::operator*(const Vector2 &v2)
{
	return Vector2(x * v2.x, y * v2.y);
}

Vector2 Vector2::operator*(float scaleFactor)
{
	return Vector2(x * scaleFactor, y * scaleFactor);
}

Vector2 Vector2::operator/(const Vector2 &v2)
{
	float dx = x;
	float dy = y;

	if (v2.x > 0.0f)
		dx = x / v2.x;

	if (v2.y > 0.0f)
		dy = y / v2.y;

	return Vector2(dx, dy);
}

void Vector2::operator+=(const Vector2 &v2)
{
	x += v2.x;
	y += v2.y;
}

void Vector2::operator-=(const Vector2 &v2)
{
	x -= v2.x;
	y -= v2.y;
}

void Vector2::operator*=(const Vector2 &v2)
{
	x *= v2.x;
	y *= v2.y;
}

void Vector2::operator/=(const Vector2 &v2)
{
	if (v2.x > 0.0f)
		x /= v2.x;

	if (v2.y > 0.0f)
		y /= v2.y;
}

bool operator<(const Vector2 &v1, const Vector2 &v2)
{
	/*float dot = Vector2::Dot(v1, v2);

	if (dot < 0.0f)
	{
		return true;
	}
	else if (dot > 0.0f)
	{
		return false;
	}
	else
	{
		return false;
	}*/

	return (v1.x < v2.x) || (v1.y < v2.y);
}

bool Vector2::operator==(const Vector2 &v2)
{
	return ((x == v2.x) && (y == v2.y));
}

bool Vector2::operator!=(const Vector2 &v2)
{
	return ((x != v2.x) || (y != v2.y));
}

//Vector2 Vector2::operator=(const Vector2 &v2)
//{
//	return Vector2{ v2.x, v2.y };
//}

Vector2 Vector2::Normalize() const
{
	Vector2 normalizedVector{ x, y };

	if (normalizedVector.x != 0.0f)
		normalizedVector.x /= normalizedVector.x;

	if (normalizedVector.y != 0.0f)
		normalizedVector.y /= normalizedVector.y;

	if (x < 0.0f)
		normalizedVector.x *= -1.0f;
	else if (y < 0.0f)
		normalizedVector.y *= -1.0f;

	return normalizedVector;
}

float Vector2::Distance(const Vector2 &v1, const Vector2 &v2)
{
	float distance = fabs(v1.x - v2.x) + fabs(v1.y - v2.y);
	return distance;
}

float Vector2::Dot(const Vector2 &v1, const Vector2 &v2)
{
	Vector2 v1Norm = v1.Normalize();
	Vector2 v2Norm = v2.Normalize();

	return ((v1Norm.x * v2Norm.x) + (v1Norm.y * v2Norm.y));
}