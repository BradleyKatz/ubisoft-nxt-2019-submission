#ifndef _VECTOR2_H
#define _VECTOR2_H

//#include "stdafx.h"

class Vector2
{
public:
	const static Vector2 down;
	const static Vector2 up;
	const static Vector2 left;
	const static Vector2 right;
	const static Vector2 zero;
public:
	float x, y;

	Vector2();
	Vector2(float x, float y);
	Vector2(const Vector2 &vec);
	//~Vector2();

	Vector2 operator+(const Vector2 &v2);
	Vector2 operator-(const Vector2 &v2);
	Vector2 operator*(const Vector2 &v2);
	Vector2 operator*(float scaleFactor);
	Vector2 operator/(const Vector2 &v2);

	void operator+=(const Vector2 &v2);
	void operator-=(const Vector2 &v2);
	void operator*=(const Vector2 &v2);
	void operator/=(const Vector2 &v2);

	friend bool operator<(const Vector2 &v1, const Vector2 &v2);
	bool operator==(const Vector2 &v2);
	bool operator!=(const Vector2 &v2);

	/*Vector2 operator=(const Vector2 &v2);*/

	Vector2 Normalize() const;

	static float Distance(const Vector2 &v1, const Vector2 &v2);
	static float Dot(const Vector2 &v1, const Vector2 &v2);
};

#endif
