#include "stdafx.h"
#include "Color.h"

const Color Color::black{ 0.0f, 0.0f, 0.0f };
const Color Color::white{ 1.0f, 1.0f, 1.0f };
const Color Color::red{ 1.0f, 0.0f, 0.0f };
const Color Color::green{ 0.0f, 1.0f, 0.0f };
const Color Color::blue{ 0.0f, 0.0f, 1.0f };
const Color Color::yellow{ 1.0f, 1.0f, 0.0f };
const Color Color::purple{ 0.6f, 0.0f, 0.9f };
const Color Color::orange{ 1.0f, 0.6f, 0.0f };
const Color Color::pink{ 1.0f, 0.5f, 0.9f };

Color::Color(): r {0.0f}, g {0.0f}, b {0.0f}
{
}

Color::Color(float r, float g, float b) : r{ r }, g{ g }, b{ b }
{
}

Color::~Color()
{
}