#ifndef _COLOR_H
#define _COLOR_H

class Color
{
public:
	const static Color black;
	const static Color white;
	const static Color red;
	const static Color green;
	const static Color blue;
	const static Color yellow;
	const static Color purple;
	const static Color orange;
	const static Color pink;

public:
	float r;
	float g;
	float b;

	Color();
	Color(float r, float g, float b);
	~Color();
};

#endif