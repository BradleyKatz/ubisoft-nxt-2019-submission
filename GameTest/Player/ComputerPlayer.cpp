#include "stdafx.h"
#include "ComputerPlayer.h"
#include "..\Systems\Collision\Physics.h"
#include "..\AI\Pathfinding.h"
#include "..\Systems\Player Manager\PlayerManager.h"

extern CSimpleTileMap g_map;

ComputerPlayer::ComputerPlayer(Vector2 position, float width, float height, Color color) : PlayerBase{ position, width, height, color }
{
	isAggressive = true;
	currentState = SeekPowerups;
}

void ComputerPlayer::Respawn()
{
	PlayerBase::Respawn();
	PathFinding::GetInstance().GeneratePath(position, currentTarget, currentPath);
}

void ComputerPlayer::ChangeTarget(const Vector2 &target)
{
	currentTarget.x = target.x;
	currentTarget.y = target.y;

	currentPath.clear();
	PathFinding::GetInstance().GeneratePath(position, target, currentPath);
	
	// Erase the beginning node (always the current position)
	if (currentPath.size() > 0)
	{
		currentPath.erase(currentPath.begin());
	}
}

void ComputerPlayer::UpdateFSM()
{
	switch (currentState)
	{
	case EAgentState::Flee:
	{
		Vector2 randomWalkable = g_map.GetRandomPosition();
		if (currentTarget != randomWalkable && currentPath.size() == 0)
		{
			ChangeTarget(randomWalkable);
		}

		break;
	}

	case EAgentState::SeekPowerups:
	{
		Vector2 powerupPos{ Vector2::zero };
		if (g_map.GetNextPowerupPosition(powerupPos))
		{
			if (currentTarget != powerupPos && currentPath.size() == 0)
			{
				ChangeTarget(powerupPos);
			}
		}
		else
		{
			Vector2 randomWalkable = g_map.GetRandomPosition();
			if (currentTarget != randomWalkable && currentPath.size() == 0)
			{
				ChangeTarget(randomWalkable);
			}
		}

		break;
	}

	case EAgentState::Chase:
	{
		Vector2 targetPos{ Vector2::zero };
		if (PlayerManager::GetInstance().GetNearestTarget(playerNum, targetPos))
		{
			if (currentTarget != targetPos)
			{
				ChangeTarget(targetPos);
			}
		}

		break;
	}
	}
}

void ComputerPlayer::UpdateMovement(float deltaTime)
{
	if (currentPath.size() > 0)
	{
		Vector2 moveAxis{ Vector2::zero };
		Vector2 moveOffset{ Vector2::zero };

		moveAxis = (currentPath[0] - position).Normalize();

		if (currentPath[0].y < position.y)
		{
			moveAxis.y *= -1.0f;
		}

		if (fabs(currentPath[0].x - position.x) > fabs(currentPath[0].y - position.y))
		{
			moveAxis.y = 0.0f;
		}
		else
		{
			moveAxis.x = 0.0f;
		}

		float speed = 0.0f;
		if (activePowerup != nullptr)
		{
			speed = POWERUP_MOVEMENT_SPEED;
		}
		else
		{
			speed = BASE_MOVEMENT_SPEED;
		}

		moveOffset = position + (moveAxis * speed * (deltaTime / 1000.0f));
		if (g_map.GetTileMapValue(moveOffset) >= EMapValue::PIP)
		{
			position += (moveAxis * speed * (deltaTime / 1000.0f));
			if (Vector2::Distance(position, currentPath[0]) < 8.0f)
			{
				currentPath.erase(currentPath.begin());
			}
		}
		else
		{
			currentPath.erase(currentPath.begin());
		}
	}
}


void ComputerPlayer::Update(float deltaTime)
{
	if (isDead)
	{
		return;
	}

	if (activePowerup != nullptr)
	{
		currentState = EAgentState::Chase;
	}
	else
	{
		if (isAggressive)
		{
			currentState = EAgentState::SeekPowerups;
		}
		else
		{
			currentState = EAgentState::Flee;
		}
	}

	UpdateFSM();
	UpdateMovement(deltaTime);
	PlayerBase::Update(deltaTime);
}