#ifndef _HUMANPLAYER_H
#define _HUMANPLAYER_H

#include "PlayerBase.h"

class HumanPlayer : public PlayerBase
{
protected:
	int controllerIndex;

public:
	HumanPlayer() = delete;
	HumanPlayer(Vector2 position, float width, float height, Color color = Color::yellow, int controllerIndex=0);

	~HumanPlayer() override;

	void Update(float deltaTime) override;
};

#endif