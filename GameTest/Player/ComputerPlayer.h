#ifndef _COMPUTERPLAYER_H
#define _COMPUTERPLAYER_H

#include "PlayerBase.h"

enum EAgentState
{
	None,
	Chase,
	Flee,
	SeekPowerups
};

class ComputerPlayer: public PlayerBase
{
protected:
	bool isAggressive = false;
	EAgentState currentState = EAgentState::Flee;
	Vector2 currentTarget;
	std::vector<Vector2> currentPath;

	void UpdateFSM();
	void UpdateMovement(float deltaTime);

public:
	ComputerPlayer() = delete;
	ComputerPlayer(Vector2 position, float width, float height, Color color = Color::yellow);

	void Respawn() override;

	void ChangeTarget(const Vector2 &target);

	void Update(float deltaTime) override;
};

#endif