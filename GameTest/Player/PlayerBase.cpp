#include "stdafx.h"
#include "PlayerBase.h"
#include "..\App\app.h"
#include "..\Systems\Collision\Physics.h"
#include "..\SimpleTileMap.h"
#include "..\Systems\Player Manager\PlayerManager.h"
#include "..\Powerup\PowerPellet.h"

extern CSimpleTileMap g_map;

const float PlayerBase::BASE_MOVEMENT_SPEED = 165.0f;
const float PlayerBase::POWERUP_MOVEMENT_SPEED = 230.0f;

PlayerBase::PlayerBase(Vector2 position, float width, float height, Color color) : position{ position }, width{ width }, height{ height }, color{ color }, collider{ nullptr }
{
}

PlayerBase::~PlayerBase()
{
	Physics::GetInstance().RemoveCollider(collider);
	delete collider;
}

void PlayerBase::Respawn()
{
	timeSpentDead = 0.0f;
	isDead = false;
	position = g_map.GetSpawnPosition(playerNum);
}

void PlayerBase::OnDeath()
{
	isDead = true;
	PlayerManager::GetInstance().UpdatePlayerLives(playerNum);

	if (activePowerup != nullptr)
	{
		delete activePowerup;
		activePowerup = nullptr;

		PlayerManager::GetInstance().ResetPowerupUI(playerNum);
	}

	if (lives == 0)
	{
		--PlayerManager::GetInstance().numPlayersLeft;
	}
}

void PlayerBase::Init()
{
	collider = new BoxCollider2D(this);
	Physics::GetInstance().RegisterCollider(collider);
}

void PlayerBase::Render()
{
	if (isDead)
	{
		return;
	}

	// In other words, treat the transform position as the center of the character
	float dx = (width / 2.0f);
	float dy = (height / 2.0f);
	
	float sx = position.x - dx;
	float sy = position.y - dy;
	float ex = position.x + dx;
	float ey = position.y + dy;

	if (activePowerup != nullptr)
	{
		activePowerup->Render();
		App::DrawQuad(sx, sy, ex, ey, activePowerup->color.r, activePowerup->color.g, activePowerup->color.b);
	}
	else
	{
		App::DrawQuad(sx, sy, ex, ey, color.r, color.g, color.b);
	}
}

void PlayerBase::Update(float deltaTime)
{
	if (!isDead)
	{
		EMapValue tileAtPosition = g_map.GetTileMapValue(position);

		switch (tileAtPosition)
		{
		case EMapValue::POWERPELLET:
		{
			g_map.OnPowerupCollected(position);
			g_map.SetTileMapValue(position, EMapValue::FLOOR);

			if (activePowerup != nullptr)
			{
				delete activePowerup;
				activePowerup = nullptr;
			}

			activePowerup = new PowerPellet();
			PlayerManager::GetInstance().UpdatePowerupUI(playerNum, activePowerup->name, activePowerup->color);

			score += 50;
			PlayerManager::GetInstance().UpdatePlayerScore(playerNum);
			break;
		}
		case EMapValue::PIP:
		{
			g_map.SetTileMapValue(position, EMapValue::FLOOR);

			score += 10;
			PlayerManager::GetInstance().UpdatePlayerScore(playerNum);

			break;
		}
		}

		if (activePowerup != nullptr)
		{
			activePowerup->elapsedTime += (deltaTime / 1000.0f);
			if (activePowerup->elapsedTime >= activePowerup->duration)
			{
				delete activePowerup;
				activePowerup = nullptr;
				PlayerManager::GetInstance().ResetPowerupUI(playerNum);
			}
			else
			{
				activePowerup->Update(deltaTime);
			}
		}
	}
}

void PlayerBase::OnCollisionEnter2D(const BoxCollider2D *collider)
{
	if (collider == nullptr || collider->attachedPlayer == nullptr || isDead || collider->attachedPlayer->isDead)
	{
		return;
	}

	if (activePowerup != nullptr && collider->attachedPlayer->activePowerup != nullptr)
	{
		// If both characters are powered up, need to determine who gets the kill if any
		// Give the kill to the player with the youngest powerup
		if (activePowerup->elapsedTime < collider->attachedPlayer->activePowerup->elapsedTime)
		{
			collider->attachedPlayer->OnDeath();

			score += 100;
			PlayerManager::GetInstance().UpdatePlayerScore(playerNum);
		}
		else
		{
			OnDeath();

			collider->attachedPlayer->score += 100;
			PlayerManager::GetInstance().UpdatePlayerScore(collider->attachedPlayer->playerNum);
		}
	}
	else if (activePowerup != nullptr)
	{
		// If only the left hand character is powered up, give them the kill
		collider->attachedPlayer->OnDeath();
		
		score += 100;
		PlayerManager::GetInstance().UpdatePlayerScore(playerNum);
	}
	else if (collider->attachedPlayer->activePowerup != nullptr)
	{
		// If only the right hand character is powered up, give them the kill
		OnDeath();

		collider->attachedPlayer->score += 100;
		PlayerManager::GetInstance().UpdatePlayerScore(collider->attachedPlayer->playerNum);
	}
}