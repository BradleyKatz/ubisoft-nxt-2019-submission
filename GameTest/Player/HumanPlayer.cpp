#include "stdafx.h"
#include "HumanPlayer.h"
#include "..\App\app.h"
#include "..\SimpleTileMap.h"

extern CSimpleTileMap g_map;

HumanPlayer::HumanPlayer(Vector2 position, float width, float height, Color color, int controllerIndex) : PlayerBase{ position, width, height, color }, controllerIndex{ controllerIndex }
{
}

HumanPlayer::~HumanPlayer()
{
}

void HumanPlayer::Update(float deltaTime)
{
	if (isDead)
	{
		return;
	}

	const CController &playerController{ App::GetController(controllerIndex) };
	Vector2 moveAxis{ Vector2::zero };

	if (playerController.CheckButton(XINPUT_GAMEPAD_DPAD_LEFT, false))
	{
		moveAxis.x = -1.0f;
	}
	else if (playerController.CheckButton(XINPUT_GAMEPAD_DPAD_RIGHT, false))
	{
		moveAxis.x = 1.0f;
	}
	else if (playerController.CheckButton(XINPUT_GAMEPAD_DPAD_UP, false))
	{
		moveAxis.y = 1.0f;
	}
	else if (playerController.CheckButton(XINPUT_GAMEPAD_DPAD_DOWN, false))
	{
		moveAxis.y = -1.0f;
	}

	float speed = 0.0f;
	if (activePowerup != nullptr)
	{
		speed = POWERUP_MOVEMENT_SPEED;
	}
	else
	{
		speed = BASE_MOVEMENT_SPEED;
	}

	Vector2 moveOffset = position + (moveAxis * speed * (deltaTime / 1000.0f));
	if (g_map.GetTileMapValue(moveOffset) >= EMapValue::PIP)
	{
		position += (moveAxis * speed * (deltaTime / 1000.0f));
	}

	PlayerBase::Update(deltaTime);
}