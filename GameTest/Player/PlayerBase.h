#ifndef _PLAYERBASE_H
#define _PLAYERBASE_H

#include "..\Common\Vector2.h"
#include "..\Common\Color.h"
#include "..\Powerup\PowerupBase.h"

class PlayerBase
{
	friend class PlayerManager;
	friend class BoxCollider2D;

protected:
	const static float BASE_MOVEMENT_SPEED;
	const static float POWERUP_MOVEMENT_SPEED;

	int lives = 0, playerNum = 0, score = 0;
	float width, height;
	Vector2 position;
	Color color;
	BoxCollider2D *collider = nullptr;
	PowerupBase *activePowerup = nullptr;

	bool isDead = false;
	float timeSpentDead = 0.0f;

public:
	PlayerBase() = delete;
	PlayerBase(Vector2 position, float width, float height, Color color=Color::yellow);

	virtual ~PlayerBase();

	virtual void Respawn();
	void OnDeath();

	void Init();
	virtual void Update(float deltaTime);
	void OnCollisionEnter2D(const BoxCollider2D *collider);

	void Render();
};

#endif