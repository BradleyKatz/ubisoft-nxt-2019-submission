#ifndef _PHYSICS_H
#define _PHYSICS_H

#include "BoxCollider2D.h"
#include "..\..\Common\Vector2.h"
#include "..\..\SimpleTileMap.h"

class Physics
{
protected:
	std::vector<BoxCollider2D*> sceneColliders;

public:
	static Physics &GetInstance();

	void RegisterCollider(BoxCollider2D *collider);
	void RemoveCollider(BoxCollider2D *collider);

	// Raycast within the tilemap
	// Return the map value found and its position
	EMapValue RaycastMap2D(const Vector2 &origin, const Vector2 &direction, float maxDistance, Vector2 *outFoundPos);

	// Raycast within the rest of the world i.e. player colliders
	bool RaycastWorld2D(const Vector2 &origin, const Vector2 &direction, float maxDistance, Vector2 *outFoundPos);

	void Update(float deltaTime);
};

#endif