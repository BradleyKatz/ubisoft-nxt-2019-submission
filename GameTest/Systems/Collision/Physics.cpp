#include "stdafx.h"
#include "Physics.h"

extern CSimpleTileMap g_map;

Physics& Physics::GetInstance()
{
	static Physics instance;
	return instance;
}

void Physics::RegisterCollider(BoxCollider2D *collider)
{
	sceneColliders.push_back(collider);
}

void Physics::RemoveCollider(BoxCollider2D *collider)
{
	for (auto itColliders = sceneColliders.begin(); itColliders != sceneColliders.end(); ++itColliders)
	{
		if ((*itColliders) == collider)
		{
			sceneColliders.erase(itColliders);
			return;
		}
	}
}

EMapValue Physics::RaycastMap2D(const Vector2 &origin, const Vector2 &direction, float maxDistance, Vector2 *outFoundPos)
{
	// Normalize the direction vector just incase
	const Vector2 dirNormalized = direction.Normalize();
	
	for (float i = 0.5f; i < maxDistance; i += 0.5f)
	{
		const Vector2 currentStep{ dirNormalized.x * i, dirNormalized.y * i };
		const Vector2 offset{ origin.x + currentStep.x, origin.y + currentStep.y };

		EMapValue tile = g_map.GetTileMapValue(offset);

		if (tile == EMapValue::BORDER || tile == EMapValue::WALL || tile >= EMapValue::POWERPELLET)
		{
			if (outFoundPos != nullptr)
			{
				outFoundPos->x = offset.x;
				outFoundPos->y = offset.y;
			}

			return tile;
		}
	}

	return EMapValue::OUTOFBOUNDS;
}

bool Physics::RaycastWorld2D(const Vector2 &origin, const Vector2 &direction, float maxDistance, Vector2 *outFoundPos)
{
	// Normalize the direction vector just incase
	const Vector2 dirNormalized = direction.Normalize();

	return false;
}

void Physics::Update(float deltaTime)
{
	for (int i = 0; i < sceneColliders.size(); ++i)
	{
		for (int j = 0; j < sceneColliders.size(); ++j)
		{
			if (i != j)
			{
				if (sceneColliders[i]->DoesIntersectCollider(sceneColliders[j]))
				{
					sceneColliders[i]->attachedPlayer->OnCollisionEnter2D(sceneColliders[j]);
				}
			}
		}
	}
}