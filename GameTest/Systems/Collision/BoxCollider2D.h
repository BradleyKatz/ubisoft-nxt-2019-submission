#ifndef _BOXCOLLIDER2D_H
#define _BOXCOLLIDER2D_H

#include "..\..\Player\PlayerBase.h"

class BoxCollider2D
{
	friend class Physics;
	friend class PlayerBase;

protected:
	PlayerBase *attachedPlayer;

public:
	BoxCollider2D() = delete;
	BoxCollider2D(PlayerBase *attachedPlayer);

	~BoxCollider2D();
	
	bool DoesIntersectCollider(const BoxCollider2D *other);
};

#endif