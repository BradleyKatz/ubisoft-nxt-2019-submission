#include "stdafx.h"
#include "BoxCollider2D.h"

BoxCollider2D::BoxCollider2D(PlayerBase *attachedPlayer) : attachedPlayer{ attachedPlayer }
{
}

BoxCollider2D::~BoxCollider2D()
{
}

bool BoxCollider2D::DoesIntersectCollider(const BoxCollider2D *other)
{
	float x = attachedPlayer->position.x;
	float y = attachedPlayer->position.y;
	float width = attachedPlayer->width;
	float height = attachedPlayer->height;

	float otherX = other->attachedPlayer->position.x;
	float otherY = other->attachedPlayer->position.y;
	float otherWidth = other->attachedPlayer->width;
	float otherHeight = other->attachedPlayer->height;

	if (x < otherX + otherWidth &&
		x + width > otherX &&
		y < otherY + otherHeight &&
		y + height > otherY)
	{
		return true;
	}
	else
	{
		return false;
	}
}