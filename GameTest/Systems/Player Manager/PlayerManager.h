#ifndef _PLAYERMANAGER_H
#define _PLAYERMANAGER_H

#include "..\..\Common\Color.h"
#include "..\..\Player\PlayerBase.h"
#include "..\UI\Player\PlayerHUD.h"

class PlayerManager
{
	friend class GameManager;
	friend class PlayerBase;

protected:
	const static int MAX_LIVES;
	const static float RESPAWN_TIME;

	const static Color P1_COLOR;
	const static Color P2_COLOR;
	const static Color P3_COLOR;
	const static Color P4_COLOR;

	int numPlayersLeft;
	std::vector<PlayerBase*> playerObjects;
	std::vector<PlayerHUD*> playerHUDObjects;

	void GetRoundWinner(Color &outWinnerColor, int &outWinnerID);

	void InitPlayerHUDs();

public:
	static PlayerManager &GetInstance();
	~PlayerManager();

	void UpdatePowerupUI(const int playerNum, const std::string &name, const Color &color);
	void ResetPowerupUI(const int playNum);
	void UpdatePlayerScore(const int playerNum);
	void UpdatePlayerLives(const int playerNum);

	// These methods assist with AI searching
	bool GetNearestThreat(const int playerNum, Vector2 &outThreatPosition);
	bool GetNearestTarget(const int playerNum, Vector2 &outTargetPosition);

	void Init(int humanPlayers, int cpuPlayers, int mapSize);
	void Update(float deltaTime);
	void Render();
	void Cleanup();
};

#endif