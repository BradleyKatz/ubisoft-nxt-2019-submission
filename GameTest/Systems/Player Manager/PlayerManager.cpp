#include "stdafx.h"
#include "PlayerManager.h"
#include "..\..\Player\HumanPlayer.h"
#include "..\..\Player\ComputerPlayer.h"
#include "..\..\App\AppSettings.h"
#include "..\..\SimpleTileMap.h"
#include <sstream>
#include <limits>

extern CSimpleTileMap g_map;

const int PlayerManager::MAX_LIVES{ 3 };
const float PlayerManager::RESPAWN_TIME{ 4.0f };

const Color PlayerManager::P1_COLOR{ Color::yellow };
const Color PlayerManager::P2_COLOR{ Color::green };
const Color PlayerManager::P3_COLOR{ Color::purple };
const Color PlayerManager::P4_COLOR{ Color::pink };

PlayerManager& PlayerManager::GetInstance()
{
	static PlayerManager instance;
	return instance;
}

PlayerManager::~PlayerManager()
{
	for (PlayerBase *player : playerObjects)
	{
		delete player;
	}

	for (PlayerHUD *hud : playerHUDObjects)
	{
		delete hud;
	}
}

void PlayerManager::GetRoundWinner(Color &outWinnerColor, int &outWinnerID)
{
	int winnerIndex = 0;
	int topScore = playerObjects[0]->score;

	for (int i = 1; i < playerObjects.size(); ++i)
	{
		if (playerObjects[i]->score > topScore)
		{
			topScore = playerObjects[i]->score;
			winnerIndex = i;
		}
	}

	outWinnerColor = playerObjects[winnerIndex]->color;
	outWinnerID = playerObjects[winnerIndex]->playerNum;
}

void PlayerManager::InitPlayerHUDs()
{
	float startX = (float)APP_VIRTUAL_WIDTH * 0.001f;
	float startY = (float)APP_VIRTUAL_HEIGHT - (float)APP_INGAME_UI_HEIGHT * 0.98;

	float hudWidth = (float)APP_VIRTUAL_WIDTH * 0.21f;
	float hudHeight = (float)APP_INGAME_UI_HEIGHT * 0.9f;

	for (int i = 0; i < playerObjects.size(); ++i)
	{
		PlayerHUD *hud = new PlayerHUD();
		hud->position.x = startX;
		hud->position.y = startY;
		hud->width = hudWidth;
		hud->height = hudHeight;

		hud->playerColor = playerObjects[i]->color;
		
		std::stringstream sstream;
		sstream << "PLAYER ";
		sstream << std::to_string(i + 1);
		hud->playerLabel = sstream.str();

		hud->SetLivesLabel(playerObjects[i]->lives);

		startX += hudWidth + (float)APP_VIRTUAL_WIDTH * 0.001f;
		playerHUDObjects.push_back(hud);
	}
}

bool PlayerManager::GetNearestThreat(const int playerNum, Vector2 &outThreatPosition)
{
	const int playerIndex = playerNum - 1;

	if (playerIndex >= 0 && playerIndex < playerObjects.size())
	{
		int threatIndex = -1;
		float shortestDist = std::numeric_limits<float>::max();

		for (int i = 0; i < playerObjects.size(); ++i)
		{
			if (i != playerIndex && playerObjects[i]->activePowerup != nullptr)
			{
				float currentDist = Vector2::Distance(playerObjects[playerIndex]->position, playerObjects[i]->position);
				if (currentDist < shortestDist)
				{
					threatIndex = i;
					shortestDist = currentDist;
				}
			}
		}

		if (threatIndex > -1)
		{
			outThreatPosition.x = playerObjects[threatIndex]->position.x;
			outThreatPosition.y = playerObjects[threatIndex]->position.y;
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool PlayerManager::GetNearestTarget(const int playerNum, Vector2 &outTargetPosition)
{
	const int playerIndex = playerNum - 1;

	if (playerIndex >= 0 && playerIndex < playerObjects.size() && playerObjects[playerIndex]->activePowerup != nullptr)
	{
		int targetIndex = -1;
		float shortestDist = std::numeric_limits<float>::max();

		for (int i = 0; i < playerObjects.size(); ++i)
		{
			if (i != playerIndex && playerObjects[i]->activePowerup == nullptr)
			{
				float currentDist = Vector2::Distance(playerObjects[playerIndex]->position, playerObjects[i]->position);
				if (currentDist < shortestDist)
				{
					targetIndex = i;
					shortestDist = currentDist;
				}
			}
		}

		if (targetIndex > -1)
		{
			outTargetPosition.x = playerObjects[targetIndex]->position.x;
			outTargetPosition.y = playerObjects[targetIndex]->position.y;
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

void PlayerManager::UpdatePowerupUI(const int playerNum, const std::string &name, const Color &color)
{
	const int playerIndex = playerNum - 1;
	
	if (playerIndex >= 0 && playerIndex < playerObjects.size())
	{
		playerHUDObjects[playerIndex]->SetPowerupUI(name, color);
	}
}

void PlayerManager::ResetPowerupUI(const int playerNum)
{
	const int playerIndex = playerNum - 1;

	if (playerIndex >= 0 && playerIndex < playerObjects.size())
	{
		playerHUDObjects[playerIndex]->ResetPowerupUI();
	}
}

void PlayerManager::UpdatePlayerScore(const int playerNum)
{
	int playerIndex = playerNum - 1;

	if (playerIndex >= 0 && playerIndex < playerObjects.size())
	{
		playerHUDObjects[playerIndex]->SetScoreLabel(playerObjects[playerIndex]->score);
	}
}

void PlayerManager::UpdatePlayerLives(const int playerNum)
{
	int playerIndex = playerNum - 1;

	if (playerIndex >= 0 && playerIndex < playerObjects.size())
	{
		--playerObjects[playerIndex]->lives;
		playerHUDObjects[playerIndex]->SetLivesLabel(playerObjects[playerIndex]->lives);
	}
}

void PlayerManager::Init(int humanPlayers, int cpuPlayers, int mapSize)
{
	numPlayersLeft = humanPlayers + cpuPlayers;

	int playerNum = 1;
	const float playerWidth = ((float)APP_VIRTUAL_WIDTH / (float)mapSize) * 0.45f;
	const float playerHeight = ((float)APP_VIRTUAL_HEIGHT / (float)mapSize) * 0.45f;

	Color playerColors[4]
	{
		P1_COLOR,
		P2_COLOR,
		P3_COLOR,
		P4_COLOR,
	};

	for (int i = 0; i < humanPlayers; ++i)
	{
		Vector2 spawnPos = g_map.GetSpawnPosition(playerNum);
		HumanPlayer *human = new HumanPlayer(spawnPos, playerWidth, playerHeight, playerColors[playerNum - 1], playerNum - 1);
		human->lives = MAX_LIVES;
		human->playerNum = playerNum;
		human->Init();

		playerObjects.push_back(human);
		++playerNum;
	}

	for (int i = 0; i < cpuPlayers; ++i)
	{
		Vector2 spawnPos = g_map.GetSpawnPosition(playerNum);
		ComputerPlayer *computer = new ComputerPlayer(spawnPos, playerWidth, playerHeight, playerColors[playerNum - 1]);
		computer->lives = MAX_LIVES;
		computer->playerNum = playerNum;
		computer->Init();

		playerObjects.push_back(computer);
		++playerNum;
	}

	InitPlayerHUDs();
}

void PlayerManager::Update(float deltaTime)
{
	for (auto player : playerObjects)
	{
		if (!player->isDead)
		{
			player->Update(deltaTime);

			if (player->activePowerup != nullptr)
			{
				playerHUDObjects[player->playerNum - 1]->UpdateElapsedTime(player->activePowerup->duration - player->activePowerup->elapsedTime);
			}
		}
		else
		{
			if (player->lives > 0)
			{
				player->timeSpentDead += (deltaTime / 1000.0f);
				if (player->timeSpentDead >= RESPAWN_TIME)
				{
					player->Respawn();
				}
			}
		}
	}

	for (auto hud : playerHUDObjects)
	{
		hud->Update(deltaTime);
	}
}

void PlayerManager::Render()
{
	for (auto player : playerObjects)
	{
		if (!player->isDead)
		{
			player->Render();
		}
	}

	for (auto hud : playerHUDObjects)
	{
		hud->Render();
	}
}

void PlayerManager::Cleanup()
{
	for (PlayerBase *player : playerObjects)
	{
		delete player;
	}

	for (PlayerHUD *hud : playerHUDObjects)
	{
		delete hud;
	}

	playerObjects.clear();
	playerHUDObjects.clear();
}