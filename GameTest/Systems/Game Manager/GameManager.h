#ifndef _GAMEMANAGER_H
#define _GAMEMANAGER_H

#include "..\UI\Button\ButtonBase.h"
#include "..\UI\Round Timer\RoundTimer.h"

enum EGameState
{
	Null,
	MainMenu,
	Ingame,
	GameOver
};

class GameManager
{
	friend class IncrementButton;

protected:
	EGameState currentState;
	int numPlayerSlotsAvailable = 4;
	int numHumanPlayers = 0, numCPUPlayers = 0;

	int mainMenuBtnIndex = 0;
	std::vector<ButtonBase*> mainMenuButtons;
	
	int roundWinnerID = -1;
	Color roundWinnerColor = Color::black;
	RoundTimer *roundTimer = nullptr;

	int gameoverMenuBtnIndex = 0;
	std::vector<ButtonBase*> gameoverMenuButtons;

	void MainMenuInit();
	void MainMenuUpdate(float deltaTime);
	void MainMenuRender();
	void MainMenuCleanup();

	void IngameInit();
	void IngameUpdate(float deltaTime);
	void IngameRender();
	void IngameCleanup();

	void GameoverInit();
	void GameoverUpdate(float deltaTime);
	void GameoverRender();
	void GameoverCleanup();

public:
	static GameManager &GetInstance();

	void ChangeGameState(EGameState state);
	
	void Update(float deltaTime);
	void Render();
};

#endif