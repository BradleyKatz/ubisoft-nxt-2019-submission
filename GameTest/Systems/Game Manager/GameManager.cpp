#include "stdafx.h"
#include "GameManager.h"
#include "..\Player Manager\PlayerManager.h"
#include "..\..\SimpleTileMap.h"
#include "..\Collision\Physics.h"
#include "..\..\App\app.h"
#include "..\..\App\AppSettings.h"
#include "..\..\Common\Color.h"
#include "..\UI\Button\Button.h"
#include "..\UI\Button\IncrementButton.h"
#include <sstream>

static const int MAP_SIZE = 12;
static const int TUNNEL_LEN = 12;
static const float TUNNEL_FILL_PERCENT = 80;

CSimpleTileMap g_map(MAP_SIZE);

GameManager& GameManager::GetInstance()
{
	static GameManager instance;
	return instance;
}

void GameManager::ChangeGameState(EGameState state)
{
	if (state != EGameState::Null && currentState != state)
	{
		switch (currentState)
		{
		case EGameState::MainMenu:
		{
			MainMenuCleanup();
			break;
		}
		case EGameState::Ingame:
		{
			IngameCleanup();
			break;
		}
		case EGameState::GameOver:
		{
			GameoverCleanup();
			break;
		}
		}

		currentState = state;

		switch (state)
		{
		case EGameState::MainMenu:
		{
			MainMenuInit();
			break;
		}
		case EGameState::Ingame:
		{
			IngameInit();
			break;
		}
		case EGameState::GameOver:
		{
			GameoverInit();
			break;
		}
		}
	}
}

void GameManager::Update(float deltaTime)
{
	switch (currentState)
	{
	case EGameState::MainMenu:
	{
		MainMenuUpdate(deltaTime);
		break;
	}
	case EGameState::Ingame:
	{
		IngameUpdate(deltaTime);
		break;
	}
	case EGameState::GameOver:
	{
		GameoverUpdate(deltaTime);
		break;
	}
	}
}

void GameManager::Render()
{
	switch (currentState)
	{
	case EGameState::MainMenu:
	{
		MainMenuRender();
		break;
	}
	case EGameState::Ingame:
	{
		IngameRender();
		break;
	}
	case EGameState::GameOver:
	{
		GameoverRender();
		break;
	}
	}
}

/*
=================
MAIN MENU METHODS
=================
*/
void GameManager::MainMenuInit()
{
	mainMenuBtnIndex = 0;
	numPlayerSlotsAvailable = 4;
	numHumanPlayers = 0;
	numCPUPlayers = 0;

	// Create num human players increment button
	{
		float btnX = (float)APP_VIRTUAL_WIDTH / 2.0f;
		float btnY = (float)APP_VIRTUAL_HEIGHT / 2.0f;
		Vector2 btnPos = Vector2(btnX, btnY);

		float btnWidth = (float)APP_VIRTUAL_WIDTH / 6.0f;
		float btnHeight = (float)APP_VIRTUAL_HEIGHT / 6.0f;

		Color btnTxtColor = Color::green;
		Color btnHighlightColor = Color::white;

		IncrementButton *humansBtn = new IncrementButton(btnPos, btnWidth, btnHeight, "PLAYERS", btnTxtColor, btnHighlightColor);
		humansBtn->isHighlighted = true;
		mainMenuButtons.push_back(humansBtn);
	}

	// Create num cpu players increment button
	{
		float btnX = (float)APP_VIRTUAL_WIDTH / 2.0f;
		float btnY = (float)APP_VIRTUAL_HEIGHT / 3.0f;
		Vector2 btnPos = Vector2(btnX, btnY);

		float btnWidth = (float)APP_VIRTUAL_WIDTH / 6.0f;
		float btnHeight = (float)APP_VIRTUAL_HEIGHT / 6.0f;

		Color btnTxtColor = Color::green;
		Color btnHighlightColor = Color::white;

		IncrementButton *cpusBtn = new IncrementButton(btnPos, btnWidth, btnHeight, "CPUS", btnTxtColor, btnHighlightColor);
		mainMenuButtons.push_back(cpusBtn);
	}

	// Create play button
	{
		float playBtnX = (float)APP_VIRTUAL_WIDTH / 2.0f;
		float playBtnY = (float)APP_VIRTUAL_HEIGHT / 6.0f;
		Vector2 playBtnPos = Vector2(playBtnX, playBtnY);

		float playBtnWidth = (float)APP_VIRTUAL_WIDTH / 6.0f;
		float playBtnHeight = (float)APP_VIRTUAL_HEIGHT / 6.0f;

		Color playBtnTxtColor = Color::green;
		Color playBtnHighlightColor = Color::white;

		Button *playButton = new Button(playBtnPos, playBtnWidth, playBtnHeight, "PLAY", playBtnTxtColor, playBtnHighlightColor);
		playButton->SetTransitionState(EGameState::Ingame);
		mainMenuButtons.push_back(playButton);
	}
}

void GameManager::MainMenuUpdate(float deltaTime)
{
	if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_UP, true))
	{
		mainMenuButtons[mainMenuBtnIndex]->isHighlighted = false;

		mainMenuBtnIndex = (mainMenuBtnIndex - 1 >= 0) ? (mainMenuBtnIndex - 1) : 0;
		mainMenuButtons[mainMenuBtnIndex]->isHighlighted = true;
	}
	else if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_DOWN, true))
	{
		mainMenuButtons[mainMenuBtnIndex]->isHighlighted = false;

		mainMenuBtnIndex = (mainMenuBtnIndex + 1 < mainMenuButtons.size()) ? (mainMenuBtnIndex + 1) : 0;
		mainMenuButtons[mainMenuBtnIndex]->isHighlighted = true;
	}

	for (auto btn : mainMenuButtons)
	{
		if (btn->buttonText == "CPUS")
		{
			numCPUPlayers = ((IncrementButton*)btn)->quantity;
		}
		else if (btn->buttonText == "PLAYERS")
		{
			numHumanPlayers = ((IncrementButton*)btn)->quantity;
		}

		btn->Update(deltaTime);
	}
}

void GameManager::MainMenuRender()
{
	// Draw game title
	float titleX = ((float)APP_VIRTUAL_WIDTH / 2.5f);
	float titleY = (float)APP_VIRTUAL_HEIGHT - (float)APP_VIRTUAL_HEIGHT / 4.0f;
	Color titleColor = Color::yellow;

	App::Print(titleX, titleY, "PAC-MAN FEVER", titleColor.r, titleColor.g, titleColor.b, GLUT_BITMAP_TIMES_ROMAN_24);

	// Draw menu buttons
	for (auto btn : mainMenuButtons)
	{
		btn->Render();
	}
}

void GameManager::MainMenuCleanup()
{
	for (ButtonBase *btn : mainMenuButtons)
	{
		delete btn;
	}

	mainMenuButtons.clear();
}


/*
==============
INGAME METHODS
==============
*/
void GameManager::IngameInit()
{
	g_map.RandomMap(TUNNEL_FILL_PERCENT, TUNNEL_LEN);
	PlayerManager::GetInstance().Init(numHumanPlayers, numCPUPlayers, MAP_SIZE);

	roundTimer = new RoundTimer();
	roundTimer->roundTime = 120.0f;
	roundTimer->pelletSpawnInterval = roundTimer->roundTime * 0.25f;
	roundTimer->elapsedTime = 0.0f;
}

void GameManager::IngameUpdate(float deltaTime)
{
	PlayerManager::GetInstance().Update(deltaTime);
	Physics::GetInstance().Update(deltaTime);

	if (roundTimer != nullptr)
	{
		roundTimer->Update(deltaTime);

		if (roundTimer->elapsedTime >= roundTimer->roundTime)
		{
			ChangeGameState(EGameState::GameOver);
		}
	}
}

void GameManager::IngameRender()
{
	g_map.Render();
	PlayerManager::GetInstance().Render();

	if (roundTimer != nullptr)
	{
		roundTimer->Render();
	}

	if (PlayerManager::GetInstance().numPlayersLeft == 1)
	{
		ChangeGameState(EGameState::GameOver);
	}
}

void GameManager::IngameCleanup()
{
	delete roundTimer;
	PlayerManager::GetInstance().GetRoundWinner(roundWinnerColor, roundWinnerID);
	PlayerManager::GetInstance().Cleanup();
}

/*
=================
GAME OVER METHODS
=================
*/

void GameManager::GameoverInit()
{
	gameoverMenuBtnIndex = 0;

	// Create Main Menu button
	{
		float btnX = (float)APP_VIRTUAL_WIDTH / 2.0f;
		float btnY = (float)APP_VIRTUAL_HEIGHT / 3.0f;
		Vector2 btnPos = Vector2(btnX, btnY);

		float btnWidth = (float)APP_VIRTUAL_WIDTH / 2.0f;
		float btnHeight = (float)APP_VIRTUAL_HEIGHT / 6.0f;

		Color btnTxtColor = Color::green;
		Color btnHighlightColor = Color::white;

		Button *mainMenuButton = new Button(btnPos, btnWidth, btnHeight, "MAIN MENU", btnTxtColor, btnHighlightColor);
		mainMenuButton->SetTransitionState(EGameState::MainMenu);
		gameoverMenuButtons.push_back(mainMenuButton);
	}
}

void GameManager::GameoverUpdate(float deltaTime)
{
	if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_UP, true))
	{
		gameoverMenuButtons[gameoverMenuBtnIndex]->isHighlighted = false;

		gameoverMenuBtnIndex = (gameoverMenuBtnIndex - 1 >= 0) ? (gameoverMenuBtnIndex - 1) : 0;
		gameoverMenuButtons[gameoverMenuBtnIndex]->isHighlighted = true;
	}
	else if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_DOWN, true))
	{
		gameoverMenuButtons[gameoverMenuBtnIndex]->isHighlighted = false;

		gameoverMenuBtnIndex = (gameoverMenuBtnIndex + 1 < gameoverMenuButtons.size()) ? (gameoverMenuBtnIndex + 1) : 0;
		gameoverMenuButtons[gameoverMenuBtnIndex]->isHighlighted = true;
	}

	for (ButtonBase *btn : gameoverMenuButtons)
	{
		try
		{
			btn->Update(deltaTime);
		}
		catch (...)
		{
			std::cerr << "Button became null during update loop" << std::endl;
		}
	}
}

void GameManager::GameoverRender()
{
	// Draw GAME OVER message
	{
		float msgX = ((float)APP_VIRTUAL_WIDTH / 2.35f);
		float msgY = (float)APP_VIRTUAL_HEIGHT - (float)APP_VIRTUAL_HEIGHT / 4.0f;

		std::stringstream sstream;
		sstream << "PLAYER ";
		sstream << std::to_string(roundWinnerID);
		sstream << " WINS";

		App::Print(msgX, msgY, sstream.str().c_str(), roundWinnerColor.r, roundWinnerColor.g, roundWinnerColor.b, GLUT_BITMAP_TIMES_ROMAN_24);

	}

	// Draw buttons
	for (ButtonBase *btn : gameoverMenuButtons)
	{
		btn->Render();
	}
}

void GameManager::GameoverCleanup()
{
	for (ButtonBase *btn : gameoverMenuButtons)
	{
		delete btn;
	}

	gameoverMenuButtons.clear();
}
