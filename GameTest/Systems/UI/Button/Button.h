#ifndef _BUTTON_H
#define _BUTTON_H

#include "ButtonBase.h"
#include "..\..\Game Manager\GameManager.h"

class Button : public ButtonBase
{
	friend class GameManager;

protected:
	EGameState transitionState;
	void SetTransitionState(const EGameState state);

public:
	Button(const Vector2 &position, const float width, const float height, const std::string &text, const Color &textColor, const Color &highlightColor, EGameState transitionState = EGameState::Null);
	~Button();

	void Update(float deltaTime) override;
	void Render() override;
};

#endif