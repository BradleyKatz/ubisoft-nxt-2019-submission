#include "stdafx.h"
#include "Button.h"
#include "..\..\..\App\app.h"

Button::Button(const Vector2 & position, const float width, const float height, const std::string & text, const Color & textColor, const Color & highlightColor, EGameState transitionState) : ButtonBase{ position, width, height, text, textColor, highlightColor }, transitionState{ transitionState }
{
}

Button::~Button()
{
}

void Button::SetTransitionState(const EGameState state)
{
	if (state != EGameState::Null)
	{
		transitionState = state;
	}
}


void Button::Update(float deltaTime)
{
	if (isHighlighted && App::GetController().CheckButton(XINPUT_GAMEPAD_A, true))
	{
		// Run button function callback if it exists
		if (transitionState != EGameState::Null)
		{
			GameManager::GetInstance().ChangeGameState(transitionState);
		}
	}
}

void Button::Render()
{
	ButtonBase::Render();
}