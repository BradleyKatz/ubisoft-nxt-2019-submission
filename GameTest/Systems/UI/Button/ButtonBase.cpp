#include "stdafx.h"
#include "ButtonBase.h"
#include "..\..\..\App\app.h"

ButtonBase::ButtonBase(const Vector2 & position, const float width, const float height, const std::string & text, const Color & textColor, const Color & highlightColor) : position{ position }, width{ width }, height{ height }, buttonText{ text }, textColor{ textColor }, highlightColor{ highlightColor }, isHighlighted{ false }
{
}

ButtonBase::~ButtonBase()
{
}

void ButtonBase::Render()
{
	// Draw outline box in white
	{
		float outlineStartX = position.x - (width / 2.0f);
		float outlineEndX = position.x + (width / 2.0f);

		float outlineStartY = position.y - (height / 4.0f);
		float outlineEndY = position.y + (height / 4.0f);

		App::DrawLine(outlineStartX, outlineStartY, outlineEndX, outlineStartY);
		App::DrawLine(outlineStartX, outlineEndY, outlineEndX, outlineEndY);
		App::DrawLine(outlineStartX, outlineStartY, outlineStartX, outlineEndY);
		App::DrawLine(outlineEndX, outlineStartY, outlineEndX, outlineEndY);
	}

	// Draw button text in the center of the bounding box
	if (isHighlighted)
	{
		App::Print(position.x - (width / 6.5f), position.y, buttonText.c_str(), highlightColor.r, highlightColor.g, highlightColor.b);
	}
	else
	{
		App::Print(position.x - (width / 6.5f), position.y, buttonText.c_str(), textColor.r, textColor.g, textColor.b, GLUT_BITMAP_TIMES_ROMAN_24);

	}
}
