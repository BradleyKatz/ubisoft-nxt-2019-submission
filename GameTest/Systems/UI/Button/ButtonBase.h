#ifndef _BUTTONBASE_H
#define _BUTTONBASE_H

#include <string>
#include "..\..\..\Common\Color.h"
#include "..\..\..\Common\Vector2.h"

class ButtonBase
{
	friend class GameManager;

protected:
	bool isHighlighted;
	float width, height;
	Vector2 position;
	std::string buttonText;
	Color textColor, highlightColor;

public:
	ButtonBase(const Vector2 &position, const float width, const float height, const std::string &text, const Color &textColor, const Color &highlightColor);
	~ButtonBase();

	virtual void Update(float deltaTime) = 0;
	virtual void Render();
};

#endif