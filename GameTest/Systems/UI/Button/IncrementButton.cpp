#include "stdafx.h"
#include "IncrementButton.h"
#include "..\..\..\App\app.h"
#include "..\..\..\Common\Color.h"
#include "..\..\Game Manager\GameManager.h"

IncrementButton::IncrementButton(const Vector2 &position, const float width, const float height, const std::string &text, const Color &textColor, const Color &highlightColor) : ButtonBase{ position, width, height, text, textColor, highlightColor }
{
}

IncrementButton::~IncrementButton()
{
}

void IncrementButton::Update(float deltaTime)
{
	if (isHighlighted)
	{
		if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_LEFT, true))
		{
			if (quantity - 1 >= 0)
			{
				quantity -= 1;
				++GameManager::GetInstance().numPlayerSlotsAvailable;
			}
		}
		else if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_RIGHT, true))
		{
			if (GameManager::GetInstance().numPlayerSlotsAvailable > 0)
			{
				quantity += 1;
				--GameManager::GetInstance().numPlayerSlotsAvailable;
			}
		}
	}
}

void IncrementButton::Render()
{
	ButtonBase::Render();

	// Draw quantity under the button label
	{
		float sx = position.x;
		float sy = position.y - (height / 6.5f);
		std::string strQuantity = std::to_string(quantity);

		if (isHighlighted)
		{
			App::Print(sx, sy, strQuantity.c_str(), highlightColor.r, highlightColor.g, highlightColor.b);
		}
		else
		{
			App::Print(sx, sy, strQuantity.c_str(), textColor.r, textColor.g, textColor.b);
		}
	}

	// Draw arrow on the left and right side of the button
	Color arrowColor = Color::yellow;

	// Draw left arrow
	{
		float sx = position.x - width;
		float sy = position.y;

		float topEX = sx + (width / 4.0f);
		float topEY = position.y - (height / 4.0f);

		float bottomEX = topEX;
		float bottomEY = position.y + (height / 4.0f);

		App::DrawLine(sx, sy, topEX, topEY, arrowColor.r, arrowColor.g, arrowColor.b);
		App::DrawLine(sx, sy, bottomEX, bottomEY, arrowColor.r, arrowColor.g, arrowColor.b);
	}

	// Draw right arrow
	{
		float sx = position.x + width;
		float sy = position.y;

		float topEX = sx - (width / 4.0f);
		float topEY = position.y - (height / 4.0f);

		float bottomEX = topEX;
		float bottomEY = position.y + (height / 4.0f);

		App::DrawLine(sx, sy, topEX, topEY, arrowColor.r, arrowColor.g, arrowColor.b);
		App::DrawLine(sx, sy, bottomEX, bottomEY, arrowColor.r, arrowColor.g, arrowColor.b);
	}
}