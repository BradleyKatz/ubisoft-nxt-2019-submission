#ifndef _INCREMENTBUTTON_H
#define _INCREMENTBUTTON_H

#include "ButtonBase.h"

class IncrementButton : public ButtonBase
{
	friend class GameManager;

protected:
	int quantity = 0;

public:
	IncrementButton(const Vector2 &position, const float width, const float height, const std::string &text, const Color &textColor, const Color &highlightColor);
	~IncrementButton();

	void Update(float deltaTime) override;
	void Render() override;
};

#endif