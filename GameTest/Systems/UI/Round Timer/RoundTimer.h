#ifndef _ROUNDTIMER_H
#define _ROUNDTIMER_H

class RoundTimer
{
	friend class GameManager;

protected:
	float pelletSpawnInterval = 0.0f;
	float roundTime = 0.0f, elapsedTime = 0.0f;

public:
	void Update(float deltaTime);
	void Render();
};

#endif