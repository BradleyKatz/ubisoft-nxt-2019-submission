#include "stdafx.h"
#include "RoundTimer.h"
#include "..\..\..\App\app.h"
#include "..\..\..\App\AppSettings.h"
#include <iomanip>
#include <sstream>
#include "..\..\..\SimpleTileMap.h"

extern CSimpleTileMap g_map;

void RoundTimer::Update(float deltaTime)
{
	if (elapsedTime < roundTime)
	{
		elapsedTime += (deltaTime / 1000.0f);

		int elapsed = static_cast<int>(elapsedTime);
		int interval = static_cast<int>(pelletSpawnInterval);

		if (elapsed > 0 && (elapsed % interval == 0))
		{
			g_map.ScatterPellets();
		}
	}
}

void RoundTimer::Render()
{
	// Draw Time label
	{
		float sx = (float)APP_VIRTUAL_WIDTH - ((float)APP_VIRTUAL_WIDTH * 0.12f);
		float sy = (float)APP_VIRTUAL_HEIGHT - ((float)APP_INGAME_UI_HEIGHT * 0.12f);

		App::Print(sx, sy, "-- TIME --");
	}

	// Draw elapsed time
	{
		int countdown = static_cast<int>(roundTime - elapsedTime);

		std::string strCountdown;
		std::stringstream sstream;
		sstream << std::setw(3) << std::setfill('0') << countdown;
		strCountdown = sstream.str();

		float sx = (float)APP_VIRTUAL_WIDTH - ((float)APP_VIRTUAL_WIDTH * 0.09f);
		float sy = (float)APP_VIRTUAL_HEIGHT - ((float)APP_INGAME_UI_HEIGHT * 0.35f);
		App::Print(sx, sy, strCountdown.c_str());
	}
}