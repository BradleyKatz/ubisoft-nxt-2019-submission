#include "stdafx.h"
#include "..\..\..\App\AppSettings.h"
#include "..\..\..\App\app.h"
#include "PlayerHUD.h"
#include <sstream>
#include <iomanip>

PlayerHUD::PlayerHUD() : position{ Vector2::zero }, width{ 0.0f }, height{ 0.0f }, elapsedTime{ 0.0f }, playerColor{ Color::black }, activePowerupColor{ Color::black }, playerLabel{ "" }, scoreLabel{ "" }, livesLabel{ "" }, activePowerupLabel{ "" }
{
}

PlayerHUD::~PlayerHUD()
{
}

void PlayerHUD::SetPowerupUI(const std::string &name, const Color &color)
{
	activePowerupLabel = name;
	activePowerupColor = color;
}

void PlayerHUD::UpdateElapsedTime(const float elapsedTime)
{
	this->elapsedTime = elapsedTime;
}

void PlayerHUD::ResetPowerupUI()
{
	activePowerupLabel = "";
	activePowerupColor = Color::black;
}

void PlayerHUD::SetScoreLabel(const int score)
{
	std::stringstream sstream;
	sstream << "Score: ";
	sstream << std::to_string(score);
	scoreLabel = sstream.str();
}

void PlayerHUD::SetLivesLabel(const int livesCount)
{
	std::stringstream sstream;
	sstream << "Lives: ";
	sstream << std::to_string(livesCount);
	livesLabel = sstream.str();
}

void PlayerHUD::Update(float deltaTime)
{
}

void PlayerHUD::Render()
{
	// Each UI item (player label, lives, etc) should take 1/5th of the height respectively
	const float uiItemHeight = height * 0.15f;

	float playerLabelStartX = position.x + (width * 0.3f);

	const float uiItemStartX = position.x + (width * 0.2f);
	float uiItemStartY = position.y + (height * 0.8f);

	const Color generalItemColor = Color::white;

	// Draw border around player HUD
	{
		App::DrawLine(position.x, position.y, position.x + width, position.y, playerColor.r, playerColor.g, playerColor.b);
		App::DrawLine(position.x + width, position.y, position.x + width, position.y + height, playerColor.r, playerColor.g, playerColor.b);
		App::DrawLine(position.x, position.y + height, position.x + width, position.y + height, playerColor.r, playerColor.g, playerColor.b);
		App::DrawLine(position.x, position.y, position.x, position.y + height, playerColor.r, playerColor.g, playerColor.b);
	}

	// Draw elements within the HUD border
	{
		// Draw player name label
		App::Print(playerLabelStartX, uiItemStartY, playerLabel.c_str(), playerColor.r, playerColor.g, playerColor.b);
		uiItemStartY -= uiItemHeight;

		// Draw score label
		App::Print(uiItemStartX, uiItemStartY, scoreLabel.c_str(), generalItemColor.r, generalItemColor.g, generalItemColor.b);
		uiItemStartY -= uiItemHeight;

		// Draw lives label
		App::Print(uiItemStartX, uiItemStartY, livesLabel.c_str(), generalItemColor.r, generalItemColor.g, generalItemColor.b);
		uiItemStartY -= uiItemHeight;

		// Draw powerup label
		if (activePowerupLabel != "")
		{
			App::Print(uiItemStartX, uiItemStartY, activePowerupLabel.c_str(), activePowerupColor.r, activePowerupColor.g, activePowerupColor.b);
			uiItemStartY -= uiItemHeight;

			std::string strCountdown;
			std::stringstream sstream;
			sstream << "REMAINING: ";
			sstream << std::setprecision(2) << elapsedTime;
			strCountdown = sstream.str();

			App::Print(uiItemStartX, uiItemStartY, strCountdown.c_str());
		}
	}
}