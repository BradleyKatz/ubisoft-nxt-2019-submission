#ifndef _PLAYERHUD_H
#define _PLAYERHUD_H

#include "..\..\..\Common\Vector2.h"
#include "..\..\..\Common\Color.h"
#include <string>

class PlayerHUD
{
	friend class PlayerManager;

protected:
	float width, height;
	float elapsedTime;
	Vector2 position;
	Color playerColor, activePowerupColor;
	std::string playerLabel, scoreLabel, livesLabel, activePowerupLabel;

public:
	PlayerHUD();
	~PlayerHUD();

	void SetPowerupUI(const std::string &name, const Color &color);
	void UpdateElapsedTime(const float elapsedTime);
	void ResetPowerupUI();
	void SetScoreLabel(const int score);
	void SetLivesLabel(const int livesCount);

	void Update(float deltaTime);
	void Render();
};

#endif